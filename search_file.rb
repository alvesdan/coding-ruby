class SearchFile

  def initialize(filename = nil)
    @path = Dir.pwd
    @filename = filename || ARGV[0]
    @files = []
  end

  def find
    @files = Dir.glob("#{@path}/**/*").find_all { |pathfile| pathfile =~ /#{@filename}/ }
    self
  end

  def output
    [
      @files.map { |file| formatted_file_name(file) },
      "Total of \e[4;32m#{pluralize(@files.size, 'file', 'files')}\e[0m found."
    ]
  end

  private

  def formatted_file_name(file)
    match_string = file.scan(/(#{@filename})/)[0][0]
    file.gsub(/(#{@filename})/, "\e[4;32m#{match_string}\e[0m")
  end

  def pluralize(counter, singular, plural)
    word = counter == 0 || counter > 1 ? plural : singular
    "#{counter} #{word}"
  end

end

puts SearchFile.new.find.output if ARGV[0]
