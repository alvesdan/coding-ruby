require 'rspec/autorun'
require './missing_term.rb'

describe MissingTerm do

  subject { MissingTerm.new }

  it 'should find missing term' do
    [
      [[1, 11, 31, 41, 51, 61], 21],
      [[1, 21, 31, 41, 51, 61], 11],
      [[1, 11, 21, 41, 51, 61], 31],
      [[1, 11, 21, 31, 51, 61], 41],
      [[1, 21, 31, 41, 51], 11],
      [[21, 31, 51], 41],
      [[2, 3, 4, 5, 6, 8, 9, 10], 7],
      [[0, 5, 10, 20, 25, 30], 15]
    ].each do |sequence|
      expect(subject.find_missing_term_in(sequence[0])).to eq sequence[1]
    end
  end

end
