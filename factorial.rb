gem "minitest"
require "minitest/autorun"

def factorial(n)
  return 1 if n < 1
  n * factorial(n - 1)
end

class FactorialTest < Minitest::Test
  
  def test_assert_factorial
    tests = [[5, 120], [6, 720], [7, 5040]]
    tests.each do |test|
      assert_equal factorial(test[0]), test[1]
    end
    
  end
  
end