Coding Ruby
===========

**These are ruby experiments!**

###### Text Calculator
Simple receive a string like `2.days * 30` and returns 60.

----------------

###### Search files in Terminal using Ruby
Use `search_file.rb` to seach for files in Terminal. You need
to configure an alias pointing to the file and that's it. It will search for
files in your current folder.

    # Example alias:
    alias rfind="ruby /code/search_file.rb"

Running `rfind .rb` in this repository folder:

    /ruby/coding-ruby/euler/1.rb
    /ruby/coding-ruby/factorial.rb
    /ruby/coding-ruby/missing_term.rb
    /ruby/coding-ruby/missing_term_spec.rb
    /ruby/coding-ruby/search_file.rb
    /ruby/coding-ruby/search_file_spec.rb
    /ruby/coding-ruby/text_calculator.rb
    /ruby/coding-ruby/text_calculator_spec.rb
    Total of 8 files found.

Now running `rfind "missing.*\.rb"`. When using **regular expression** you must use `""`.

    /ruby/coding-ruby/missing_term.rb
    /ruby/coding-ruby/missing_term_spec.rb
    Total of 2 files found.
