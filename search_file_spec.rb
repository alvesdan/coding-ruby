require 'rspec/autorun'
require './search_file.rb'

describe SearchFile do

  # TODO: Ensure tests that don't depends on system files on this folder
  # TODO: Write better specs :/

  let(:readme_output) { SearchFile.new('README.md').find.output.join }
  let(:findme_output) { SearchFile.new('.*\.txt').find.output.join }
  let(:text_calculator_output) { SearchFile.new('text_calculator').find.output.join }

  it 'should find files in first folder level' do
    expect(readme_output).to match(/README.md/)
    expect(readme_output).to match(/1 file.*found/)
  end

  it 'should find files in deep folder level' do
    expect(findme_output).to match(/tmp\/example\/nested\/.*findme.txt/)
  end

  it 'should return the correct number of files found' do
    expect(text_calculator_output).to match(/2 files.*found/)
  end

end
