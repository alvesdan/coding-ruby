# discover the missing term in the sequences

class MissingTerm

  def find_missing_term_in(sequence)
    length = sequence.length
    ratio = 100_000_000
    (1..length).each do |n|
      index = length - n
      if index > 0
        difference = sequence[index] - sequence[index-1]
        ratio = difference if (difference < ratio)
      end
    end
    (0..length).each do |n|
      correct = sequence[0] + ((n+1) - 1) * ratio
      return correct if sequence[n] != correct
    end
  end

end
