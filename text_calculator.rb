class TextCalculator

  attr_reader :input

  def initialize(input)
    @input = input
  end

  def calculate
    eval(filtered_input)
  end

  def filtered_input
    @input.gsub(/[^\*+0-9]/, '')
  end

end

class TerminalCalculator
  def initialize(output = $stdout)
    @output = output
  end
  def input
    @output.puts 'Type:'
    gets.chomp
  end
  def run
    @output.puts "Result: #{TextCalculator.new(input).calculate}"
  end
end

TerminalCalculator.new.run unless defined?(RSpec)
