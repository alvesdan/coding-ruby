require 'rspec/autorun'
require 'stringio'
require './text_calculator.rb'

describe TextCalculator do

  let(:input) { '2.days * 30' }
  subject { TextCalculator.new(input) }

  it 'should set input value' do
    expect(subject.input).to eq input
  end

  it 'should return evaluated value from input' do
    subject.stub(:filtered_input).and_return('2*30')
    expect(subject.calculate).to eq 60
  end

  it 'should filter inputed string' do
    expect(subject.filtered_input).to eq '2*30'
  end

end

describe TerminalCalculator do

  let(:output) { StringIO.new }
  let(:input) { '2.days * 30' }
  before :each do
    subject.stub_chain(:gets, :chomp).and_return(input)
  end

  subject { TerminalCalculator.new(output) }

  it 'should gets terminal input' do
    subject.should_receive(:gets)
    subject.input
  end

  it 'should print result' do
    subject.run
    output.seek(0)
    expect(output.read).to match(/Result\: 60/)
  end

end
